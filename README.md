# Projet iut-sondages
### Dans le cadres des études à l'IUT informatique d'Orléans

Projet réaliser par groupes de 2. Antoine Lassier et Sébastien Gedeon. Rendu le _27 mars 2015_.

## Sujet

Le but est de réaliser une application de gestion de sondages en utilisant une architecture REST et un client riche en JavaScript.
L'application doit essentiellement être de type **one page** (au moins pour la partie gestion des questions et visualisation des statistiques associées à un sondage).

Fonctionalitées attendues :
+ La création d'un sondage avec sa liste de questions
+ Des questions de type `SimpleQuestion`, `YesNoQuestion` ou `MultiplesAlternativesQuestion`
+ L'ajout d'une question ou d'un sondage utilisera des technologies de type _ajax_
+ Il faut prévoir des routes, vues et templates pour passer les sondages
+ Des route sont ajoutées dans l'API pour lire les résultats des sondages
+ L'application doit permettre de visualiser les résultats sous forme graphique (camenberts, histogrammes, etc.)

Extensions possibles :
+ Gérer les utilisateurs qui seront créateurs de sondages et leur authentification (sur l'API REST aussi)
+ Ajouter d'autre types de questions (réponses en texte ou numériques par exemple) et des fonctionnalités commes des plages d'ouverture des sondages

## Quickstart

Il est recommandé de créer un environnement virtuel Python pour faciliter le développement :

```bash
virtualenv venv
source venv/bin/activate
```


Le fichier `requirements.txt` contient toutes les dépendances requises par le projet, pour les installer, il faut utiliser la commande :

```bash
pip install -r requirements.txt
```


Il faut ensuite créer la base de données, un fichier `sondage/sondage1.json` est fourni comme base. Pour le charger il faut utiliser la commande :

```bash
./manage.py loaddb sondages/sondage1.json
```


Vous pouvez ensuite lancer le projet grâce à la commande

```bash
./manage.py runserver
```


Voici toutes les commandes de `manage.py` :

| Commande    | Descritption |
| ----------- | ------------ |
| runserver   | Lance un serveur de développement pour accéder au projet |
| loaddb      | Crée les tables dans un fichier `quest.db` depuis un fichier json passé en paramètre |
| shell       | Lance un shell dans le contexte de l'application |