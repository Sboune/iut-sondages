from .app import db

class Sondage(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(100))

  def __init__(self, name):
    super(Sondage, self).__init__()
    self.name = name

  def __repr__(self):
    return "<Sondage (%d) %s>" % (self.id, self.name)

class Question(db.Model):

  id = db.Column(db.Integer, primary_key=True)
  title = db.Column(db.String(120))
  sondage_id = db.Column(db.Integer, db.ForeignKey('sondage.id'))
  sondage = db.relationship("Sondage", backref=db.backref("questions", lazy="dynamic", cascade="all, delete-orphan"))

  db.__mapper_args__ = {
      'polymorphic_identity':'question',
      'polymorphic_on':type
  }

  def __repr__(self):
    return "<Question (%d) %s>" % (self.id, self.title)

  def toDict(self):
    return { 
              'id': self.id, 
              'title': self.title 
            }

class SimpleQuestion(Question):

  firstAlternative = db.Column(db.String(500))
  secondAlternative = db.Column(db.String(500))

  def __init__(self, title,  firstAlternative, secondAlternative, id):
    super(SimpleQuestion, self).__init__()
    self.title = title
    self.firstAlternative = firstAlternative
    self.secondAlternative = secondAlternative
    self.sondage_id = id

  db.__mapper_args__ = {
      'polymorphic_identity':'simplequestion'
  }

  def toDict(self):
    return { 
              'id': self.id, 
              'title': self.title, 
              'firstAlternative': self.firstAlternative, 
              'secondAlternative': self.secondAlternative 
            }

def getAllQuestions():
  return Question.query.all()

def addQuestion(id, title, firstAlternative, secondAlternative):
  q = SimpleQuestion(title, firstAlternative, secondAlternative, id)
  db.session.add(q)
  db.session.commit()

def getQuestionsSlice(debut = 0, nombre = 10):
  return 