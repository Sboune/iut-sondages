from .app import app
from flask import render_template, jsonify, request
from .models import Question, Sondage, SimpleQuestion, getAllQuestions, addQuestion

@app.route("/")
def home():
	return render_template(
		"flex.html",
		title="Questions")

questions = [
              {
                'id': 1,
                'title': u'Activité programmation',
                'firstAlternative': u'Vous faites du Java',
                'secondAlternative': u'Vous préférez Python'
              },
              {
                'id': 2,
                'title': u'Activité Montagne:',
                'firstAlternative':  u'Vous allez à la montagne',
                'secondAlternative': u'Vous préférez la plaine'
              }
            ]

@app.route("/api/questions", methods=['GET'])
def get_questions():
  return jsonify({ 'questions': [x.toDict() for x in getAllQuestions()] })

@app.route("/api/questions", methods=['POST'])
def create_task():
  if not request.json or not 'title' in request.json:
    abort(400)
  question = {
    'id': questions[-1]['id'] + 1,
    'title': request.json['title'],
    'firstAlternative': request.json.get('firstAlternative', ""),
    'secondAlternative': request.json.get('secondAlternative', "")
  }
  addQuestion(question['id'], question['title'], question['firstAlternative'], question['secondAlternative'])
  return jsonify({ 'question': question }), 201