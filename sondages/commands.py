from .app import manager, db
from .models import Sondage, Question, SimpleQuestion
import json

@manager.command
def loaddb(filename):
  '''
  Cree les tables et les remplit avec 
  les données du fichier json
  '''

  # Création de toutes les tables
  db.create_all()

  with open(filename) as json_file:
    sondage = json.load(json_file)

  sond = Sondage(name=sondage["name"])
  db.session.add(sond)
  db.session.commit()

  for q in sondage["questions"]:
    oq = SimpleQuestion(title=q["title"], firstAlternative=q["firstAlternative"], secondAlternative=q["secondAlternative"], id=sond.id)
    db.session.add(oq)
  db.session.commit()

  
  
